## Terms and Conditions ##

By using these, you acknowledge the following:

This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants you a nonexclusive copyright license to use all programming code examples from which you can generate similar functionality tailored to your own specific needs.  

These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for a particular purpose are expressly disclaimed.

License agreement can be found here: https://success.scribesoft.com/s/article/ka632000000GmxZAAS/Scribe-Software-Tool-Kit-And-Technology-License-Agreement

## Powershell Example Scripts to monitor and start Scribe Solutions ##

This Script is being provided to assist you in monitoring and starting your soltuions outside of the Scribe Online UI.  This example shows how to Start 1 Integration solution and check to see if the solution started successfully or not.
An email will be sent to inform you if the solution is in good shape. It is highly configurable and customizable to easily expand its purpose to fit your needs.

