<#

Terms and Conditions
By using these, you acknowledge the following:
This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants you a nonexclusive copyright license to use 
all programming code examples from which you can generate similar functionality tailored to your own specific needs. 
These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without any warranties of any kind. Therefore, 
Scribe cannot guarantee or imply reliability, serviceability, or functionality of these programs. The implied warranties of non-infringement, merchantability,
 and fitness for a particular purpose are expressly disclaimed.
License agreement can be found here: https://success.scribesoft.com/s/article/ka632000000GmxZAAS/Scribe-Software-Tool-Kit-And-Technology-License-Agreement



Use case of this Script:

Start solution and check if it started
Solution Concept: 
1.     Start 1 Integration solution. 
2.     Check to see if the solution started successfully or not. Allow configuration of a variable in the script to act as a wait time before checking the status of the job.
3.     If the Solution has started successfully, 
a.      send email for Successful Start, via the customer’s provided SMTP server and account to a list of email addresses on the status of whether the solution started or not. 
		In the email include the solution name and the startup status. 
4.     else 
a.      send email for failed start via the customer’s provided SMTP server and account to a list of email addresses on the status of whether the solution started or not.
		In the email include the solution name and the startup status.





These are input parameters to have values passed into them from an outside script or console command. 
Scribe Online $user, $pass, $orgId, and $solId are mandatory values in order run the script.
To pass these in via Powershell open a PS window navigate to the psl directory and run the following command:

./solution.psl -user "Scribe Online User" -pass "Scribe Online Password" -orgId "Scribe online orgID" -solId "Scribe Solution ID"

To run this from a batch file or another automated file you can save the file to the same directory as this PS file and can use the following format. In a Batch file for example you would use:

Powershell.exe -File solution.ps1 -user "Scribe Online User" -pass "Scribe Online Password" -orgId "Scribe online orgID" -solId "Scribe Solution ID"

If you would like to get an email on if the solution started correctly then follow the same format for the email information from the variables below. 
All of the parameters below are configurable via command line like above

Parameters with the Mandatory=$true above them are values that have to be passed into powershell in order for the script to work. These params are:

$user, $pass, $ordId, $solId

To find your orgId and Solution ID you can go to your org and solution in the Scribe Online UI and copy them from the URL 
For Example app.scribesoft.com/#!/solutionedit?orgId=0000000&solutionId=a517182f-88d7-4591-a05b-108f77127. 
The OrgId is 0000000 and solution ID is the string after the = at the end
#>
param(
	  [Parameter(Position=0,Mandatory=$true)]
	  #$user should be your scribe online admin user email like Scribe@example.com
	  [string]$user,
	  [Parameter(Position=1,Mandatory=$true)]
	  #$pass is your password for the user
	  [string]$pass,
	 
	  
	  [Parameter(Position=2,Mandatory=$true)]####
	  [Int32]$orgId = 16251,
	  [Parameter(Position=3,Mandatory=$true)]####
	  [string]$solId = "f064ac4d-1f7b-4673-978b-",
	  #IF you would like to recieve an email letting you know if the solution started succesfully then you can pass in the $emailuser and $emailpass parameters
	  #as the email address you would like to recieve the notification from
	  [string]$emailFrom = "",
	  [string]$emailpass = "",
	  [string]$emailTo ="",
	  [string]$emailCc = "bryan.sopko@scribesoft.com",
	  [string]$emailSubject = "Your solution information is ready",
	  [string]$smtp = "smtp.gmail.com",
	  [Int32]$Port = 587,
	  #This configures how long to wait in seconds before getting the status of the job after trying to start the solution
	  [Int32]$waitime = 5
	  )
#To automate this you can assign variables in another file or powershell by calling the PS script and using -user 'email@email' -pass 'password' and so on
#This variable pairs the Scribe Online username and password to send the web request
$credPair = "$($user):$($pass)"
# Encode the paired username and password to Base64 string
$encodedCredentials = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($credPair))
# The value of the Authorization header is set to basic of the encoded creds
$headers = @{ Authorization = "Basic $encodedCredentials" }
#The url variable is the api request for starting a job and getting the status. It adds the organization ID and Solution ID to the String
$url = "https://api.scribesoft.com:443/v1/orgs/"+$orgId+"/solutions/"+$solId

#Function to send email via SMTP
function Send-ToEmail([string]$status){
	#Assign email to send it from, who to send it to, the subject, what to put in the body, the smtpServer settings, and the credentials of the email to send from
	$MailArgs = @{
		From       = $emailFrom
		To         = $emailTo
		Cc		   = $emailCc
		Subject    = $emailSubject
		Body       = $status
		SmtpServer = $smtp
		Port       = $Port
		UseSsl     = $true
		Credential = New-Object pscredential $emailFrom,$($emailpass |ConvertTo-SecureString -AsPlainText -Force)
	}
	#Sends the mail message
	Send-MailMessage @MailArgs
	Write-Host "Email Sent"
 }
function Start-Solution{
#Hashtable that allows for a simple call to include all arguments needed  for the Api request. $StartArgs starts the solution job
	$StartArgs = @{
		Uri       = $url+"/start"	
		Headers   = $headers 
		Method = 'Post'
	}
	try{
		
		$response = Invoke-WebRequest @StartArgs
	}
	#IF there is an error when making the request send an email saying what the error was

	catch [System.Net.WebException] {
		If  ($_.Exception.Message) {
		$errMsg = ($_.Exception.Message).ToString().Trim() +". This occured when trying to start the solution with an API request.";
		Send-ToEmail -status $errMsg;
		}
	}
} 
function Status-Solution{
#$StatusArgs gets the Status of the solution 
$StatusArgs = @{
	Uri       = $url	
	Headers   = $headers 
	Method = 'GET'
}
	#Get Status of the solution I.E. Starting, Preparing, Failed, Started
	<#Get the status back and just show the status. This can also be configured to show the following
	"id": "Solutin ID",
    "name": "Solution Name",
    "agentId": "Agent ID",
    "description": "Description of Solution",
    "connectionIdForSource": "Source connection ID",
    "connectionIdForTarget": Target connection ID",
    "solutionType": "Integration or Replication",
    "status": "Starting,Preparing, OnDemand, ETC",
    "inProgressStartTime": "Start time of job",
    "lastRunTime": "Last time job was ran",
    "nextRunTime": "Next Time job should run",
	
	To implement any of these add them to the select statment below I.E. select status, id, name, agentid
	#>
	#Calls the Send-ToEmail function and includes the status of the job in the body of the email.
	Try{
		$response = Invoke-WebRequest  @StatusArgs
		$status = $response.Content | ConvertFrom-Json | select name, status, lastRunTime #|out-string #Change formatting for email
		Write-Host $status
		Send-ToEmail -status $status
	}
	#IF there is an error when making the request send an email saying what the error was
	catch [System.Net.WebException] {
		If  ($_.Exception.Message) {
		$errMsg = ($_.Exception.Message).ToString().Trim() +". This occured when trying to get the status of the solution with an API request";
		Send-ToEmail -status $errMsg;
		}
	}
} 
      


#Go to the Start-Solution and Status-Solution function to run the process of starting a solution and getting the status
Start-Solution 
#Wait for X seconds until we check if the solution started successfully or not
Start-Sleep -s $waitime
Status-Solution 













