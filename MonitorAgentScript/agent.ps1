<#

Terms and Conditions
By using these, you acknowledge the following:
This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants you a nonexclusive copyright license to use 
all programming code examples from which you can generate similar functionality tailored to your own specific needs. 
These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without any warranties of any kind. Therefore, 
Scribe cannot guarantee or imply reliability, serviceability, or functionality of these programs. The implied warranties of non-infringement, merchantability,
 and fitness for a particular purpose are expressly disclaimed.
License agreement can be found here: https://success.scribesoft.com/s/article/ka632000000GmxZAAS/Scribe-Software-Tool-Kit-And-Technology-License-Agreement


Use case of this Script:

Check if agent is running or in an unresponsive state
Solution Concept: 
1. Send a command through the API to the agent to see if the agent is responsive.  
2. Allow configuration of the script to have a list of agents to do this test with. The test supports multiple agents. The list was made to be by agent name. 
3. If the agent responded successfully, 
a. send email for a successful response, via the customer’s provided SMTP server and account to a list of email addresses on the status of the agent. In the email include the agent names and status  
4. else
a. send email for failed response, via the customer’s provided SMTP server and account to a list of email addresses on the status of the agent. In the email include the agent names and status

These are input parameters to have values passed into them from an outside script or console command. 
To pass these in via Powershell open a PS window navigate to the psl directory and run the following command:

./agent.psl -user "Scribe Online User" -pass "Scribe Online Password" -orgId "Scribe online orgID" -AgentName "Scribe Agent" -conname "Connection name to test against agent" 

To run this from a batch file or another automated file you can save the file to the same directory as this PS file and can use the following format. In a Batch file for example you would use:

Powershell.exe -File agent.ps1 -user "Scribe Online User" -pass "Scribe Online Password" -orgId "Scribe online orgID" -AgentName "Scribe Agent Name" -conname "Connection name to test against agent" 

If you would like to get an email on if the solution started correctly then follow the same format for the email information from the variables below. 
All of the parameters below are configurable via command line like above

Parameters with the Mandatory=$true above them are values that have to be passed into powershell in order for the script to work. These params are:

$user, $pass, $ordId, $AgentName, $conname
#>
param(
	  [Parameter(Position=0,Mandatory=$true)]
	  #$user should be your scribe online admin user email like Scribe@example.com
	  [string]$user,
	  [Parameter(Position=1,Mandatory=$true)]
	  #$pass is your password for the user
	  [string]$pass,
	  #To find your orgId and Solution ID you can go to your org and solution in the Scribe Online UI and copy them from the URL 
	  #For Example app.scribesoft.com/#!/solutionedit?orgId=0000000&solutionId=a517182f-88d7-4591-a05b-108f77127. 
	  #The OrgId is 0000000 and solution ID is the string after the = at the end
	  [Parameter(Position=2,Mandatory=$true)]
	  [Int32]$orgId = 16251,
	  [Parameter(Position=3,Mandatory=$true)]
	  #To pass in multiple agents the format should be -AgentName ("Agent 1", "Agent 2", "Agent 3", etc)
	  [string[]]$AgentName = @("IE8WIN7 Agent"),
	  [Parameter(Position=4,Mandatory=$true)]
	  #In order to see if the agent is responsive a connection must be configured to test against like a CRM connection, Salesforce, SQL, etc where the agent is able to successfully test against that connection.
	  [string]$conname = "CRMNEW",
	  #IF you would like to recieve an email letting you know the agent status then you can pass in the $email smtp seetings below parameters
	  #as the email address you would like to recieve the notification from. To email multiple recipients
	  #change the variable to look like [string[]] $emailTo = "email 1", "email 2", etc
	  [string]$emailFrom = "",
	  [string]$emailpass = "",
	  [string]$emailTo ="",
	  [string]$emailCc = "bryan.sopko@scribesoft.com",
	  [string]$emailSubject = "Your agent information is ready",
	  [string]$smtp = "smtp.gmail.com",
	  [Int32]$Port = 587,
	  #wait for x seconds before making another api call to see if agent was succesful
	  [Int32]$waitime = 10,
	  #Timeout if no response was heard in the last 30 seconds
	  [Int32]$timeout = 30

	  )
#This variable pairs the Scribe Online username and password to send the web request
$credPair = "$($user):$($pass)"
# Encode the paired username and password to Base64 string
$encodedCredentials = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($credPair))
# The value of the Authorization header is set to basic of the encoded creds
$headers = @{ Authorization = "Basic $encodedCredentials" }
#The url variables is the api request for getting the agent information, connection information, and testing the connection
$url = "https://api.scribesoft.com:443/v1/orgs/"+$orgId+"/agents?name="
$conurl = "https://api.scribesoft.com:443/v1/orgs/"+$orgId+"/connections?name="+$conname+"&agentId="
$testurl = "https://api.scribesoft.com:443/v1/orgs/"+$orgId+"/connections/"
$StatusArgs = @{	
		Headers   = $headers 	
}
#Function to send email via SMTP
function Send-ToEmail([string]$status){
	#Assign email to send it from, who to send it to, the subject, what to put in the body, the smtpServer settings, and the credentials of the email to send from
	$MailArgs = @{
		From       = $emailFrom
		To         = $emailTo
		Cc		   = $emailCc
		Subject    = $emailSubject
		Body       = $status
		SmtpServer = $smtp
		Port       = $Port
		UseSsl     = $true
		Credential = New-Object pscredential $emailFrom,$($emailpass |ConvertTo-SecureString -AsPlainText -Force)
	}
	#Sends the mail message
	Send-MailMessage @MailArgs
	Write-Host "Email Sent"
 }
 
function Agent-Status{
	#$StatusArgs gets the Status of the agent(s)
	
	
	#Get Status of the Agent
	<#Get the status back and just show the status. This can also be configured to show the following
	"id": "7e7cc6dc-a6ba-47ee-a2a8-e17cc6d8a",
    "name": "GUVM-DEV01 Agent",
    "status": "HeartbeatFailed",
    "machineName": "GUVM-DEV01",
    "version": "1.0.1.6272",
    "serviceName": "Scribe Online Agent",
    "lastStartTime": "2018-01-08T15:02:57.427Z",
    "lastShutdownTime": "2018-01-08T15:01:35.73Z",
    "lastContactTime": "2018-01-08T16:38:44.353Z",
    "isCloudAgent": false,
    "isUpdating": false,
    "usedInSolutions": "Insight MySQL Server"
	
	To implement any of these add them to the -Property statment below I.E. select, status, id, name, agentid
	#>
	try{
	#do a web request for each agent in the agent name array that you want to have information outputted for
	foreach ($agent in $AgentName){
		$response = Invoke-WebRequest -Uri $url$agent @StatusArgs
	    $agentid = $response.Content | ConvertFrom-Json 
		$agentid = $agentid.id
		Write-Host "AgentId "$agent
		$status = $response.Content | ConvertFrom-Json | Format-Table -Wrap -AutoSize -Property name, status, lastRunTime |out-string
		#pass the retrieved agent id to est the connection
		Connection-Test -id $agentid
	}
	}
	#IF there is an error when making the request send an email saying what the error was
	catch [System.Net.WebException] {
		If  ($_.Exception.Message) {
		$errMsg = ($_.Exception.Message).ToString().Trim() +". This occured when trying to get the status of the agent with an API request";
		Send-ToEmail -status $errMsg;
		}
	}
} 
#this function handles getting the command ID for the connection and tess the connection
function Connection-Test($id){
	#First get the connection id to test the connection (This really tests if the agent is responsive)
	# Then we use the connection id to get the command id which is in charge of getting the response back from the test connection
	Write-Host $conurl$id
	$response = Invoke-WebRequest -Uri $conurl$id  @StatusArgs
	$conid = $response.Content | ConvertFrom-Json 
	$conid = $conid.id+"/test?agentId="+$id
	$response = Invoke-WebRequest -Uri $testurl$conid  @StatusArgs -Method "Post"
	$commId = $response.Content | ConvertFrom-Json
	$commId = $commId.id
	#Write-Host $testurl"/test/"$commId
	$testurl = $testurl+"/test/"+$commId
	#Start a stopwatch that will allow this to timeout if agent is not responding. To set the timeout use $timeout. Default is 30 seconds
	$stopwatch =  [system.diagnostics.stopwatch]::StartNew()
	#Below checks if the status is processing then continue checking on the status until it changes to something else.
	#Wait for the waittime (Default 5 seconds) between each api call so we do not hammer the API
	do{
		Write-Host "Waiting"
		Start-Sleep -s $waitime
		Write-Host "Waited"
		#IF there was an error when trying to reach the api then send an email
		try{
		$response = Invoke-WebRequest -Uri $testurl @StatusArgs
		}
		catch [System.Net.WebException] {
		If  ($_.Exception.Message) {
			$errMsg = ($_.Exception.Message).ToString().Trim() +". This occured when trying test connection with an API request.";
			Send-ToEmail -status $errMsg;
		}
		}	
		#Get the status of the test connection (Processing, success, etc)
		$testcon = $response.Content | ConvertFrom-Json
		$teststat = $testcon.status
		#Write-Host "Value is: "$testcon
		#If the status is equal to processing and we have not reached the timeout then continue
		#$totalSecs = [math]::Round($stopwatch.Elapsed.TotalSeconds,0)
	}while($teststat -eq 'Processing' -or $stopwatch.Elapsed -lt $timeout) 
		$stopwatch.Stop()
		$totalSecs = [math]::Round($stopwatch.Elapsed.TotalSeconds,0)
		#If there was a timeout set the status to say something was wrong with the agent because it couldn't leave that state
		if ($totalSecs -ge $timeout){
			$status = "Timeout occured when testing the connection. This could be due to agent failure."
		}
		#Otherwise send an email of the success
		else{
			Write-Host "Exit"
			$status = $testcon | select name, reply, status 
			Write-Host 
		}
		Send-ToEmail -status $status
}

Agent-Status













